package Control;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Action.Action;
import Action.DetailFormAction;
import Action.InsertFormAction;
import Action.ListAction;
import Action.writeAction;
@WebServlet("/board.do")
public class MyControl extends HttpServlet{
      @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    	// TODO Auto-generated method stub
    	doProcess(req, resp);
    }
      private void doProcess(HttpServletRequest req, HttpServletResponse resp) {
		String cmd= req.getParameter("cmd");
		Action model = null;
	   if(cmd ==null||cmd.equalsIgnoreCase("list")){
		  //전체목록 보기 
		   model = new ListAction();
	   }else if(cmd.equalsIgnoreCase("insertForm")){
		  //입력 양식보기 
		     model = new InsertFormAction();
		   //board/jsp/insertForm.jsp이동
	   }else if(cmd.equalsIgnoreCase("insert")){
		  //입력 
		   model=new writeAction();
	   }else if(cmd.equalsIgnoreCase("detail")){
		  //상세보기 
		   model=new DetailFormAction();
	   }else if(cmd.equalsIgnoreCase("delete")){
		  //삭제 
	   }
		String viewPage=model.execute(req, resp);
		
		RequestDispatcher rd = 
				req.getRequestDispatcher(viewPage);
				try {
					rd.forward(req, resp);
				} catch (ServletException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	}
	@Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    	// TODO Auto-generated method stub
    	  doProcess(req, resp);
    }
}
