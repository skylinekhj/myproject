package Action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Board.BoardDAO;
import Board.BoardVO;

public class writeAction implements Action{

	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp) {
		String title = req.getParameter("title");
		String contents = req.getParameter("contents");
		String writer = req.getParameter("writer");
		String ip = req.getRemoteAddr();
		BoardDAO dao = new BoardDAO();
		BoardVO vo = new BoardVO();
		vo.setContents(contents);
		vo.setTitle(title);
		vo.setWriter(writer);
		vo.setIp(ip);
		dao.insertData(vo);
		return "board/jsp/insertOk.jsp";
	}

}