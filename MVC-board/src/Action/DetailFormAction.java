package Action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Board.BoardDAO;
import Board.BoardVO;

public class DetailFormAction implements Action {

	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp) {
		String b = req.getParameter("bno");
		if(b!=null){
			int bno =Integer.parseInt(b);
			BoardDAO dao = new BoardDAO();
			BoardVO vo = dao.getData(bno);
			req.setAttribute("vo", vo);
		}
		return "board/jsp/detail.jsp";
	}

}
