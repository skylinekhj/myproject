package Action;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Board.BoardDAO;
import Board.BoardVO;

public class ListAction implements Action{
	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp) {
		// db 전체 목록을 얻어서
		// 요청 객체의 속성에 set
		BoardDAO dao = new BoardDAO();
		ArrayList<BoardVO> list
		=dao.getAllData();
		req.setAttribute("list", list);
		return "board/jsp/list.jsp";
	}

}