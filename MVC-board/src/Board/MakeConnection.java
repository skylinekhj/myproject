package Board;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MakeConnection {
	//싱글톤 패턴을 적용
	private static MakeConnection mc;
	private static Connection conn;
	final String DRIVER="oracle.jdbc.driver.OracleDriver";
	final String URL="jdbc:oracle:thin:@orcl.cdxdvaaum4mv.us-west-2.rds.amazonaws.com:1521:orcl";
	final String USER="happyhbi";
	final String PASSWORD="hbi150629";

private MakeConnection(){
	
}
public static MakeConnection getInstance(){
if(mc==null) mc=new MakeConnection();

return mc;
}
public Connection getConnection(){
	if(conn == null){
		try {
			Class.forName(DRIVER);
			conn= DriverManager.getConnection(URL, USER, PASSWORD);
		} catch (ClassNotFoundException e) {
			System.out.println("드라이버 로딩 실패");
		} catch (SQLException e) {
			System.out.println("DB연결실패");
			e.printStackTrace();
		}
	}
	return conn;
}
}
