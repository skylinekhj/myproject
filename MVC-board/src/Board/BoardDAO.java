package Board;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import Board.MakeConnection;
import Board.BoardVO;

public class BoardDAO {
	private static StringBuffer sql = new StringBuffer(); 
	private static Connection conn = null;
	private static PreparedStatement pstmt = null;
	private static ResultSet rs = null;
	
	public BoardDAO(){
		conn = MakeConnection.getInstance().getConnection();
	}
	
	public ArrayList<BoardVO> getAllData(){
		ArrayList<BoardVO> list = new ArrayList<BoardVO>();
		sql.setLength(0);
		sql.append("select * from board");
		
		try {
			pstmt = conn.prepareStatement(sql.toString());
			rs = pstmt.executeQuery();
			
			while(rs.next()){
				BoardVO vo = new BoardVO(rs.getInt("bno"), rs.getString("title"), rs.getString("contents"), rs.getString("writer"), rs.getString("ip"), rs.getInt("hits"), rs.getString("wdate"));
				list.add(vo);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public ArrayList<BoardVO> getListData(int start,int end){
		ArrayList<BoardVO> list = new ArrayList<BoardVO>();
		sql.setLength(0);
		sql.append("select bno, title, contents, writer, ip, hits, wdate from ");
		sql.append("(select rownum rn, bno, title, contents, writer, ip, hits, wdate from ");
		sql.append("(select * from board order by wdate desc) ");
		sql.append("where rn < ?) ");
		sql.append("where rn > ?");
		
		try {
			pstmt = conn.prepareStatement(sql.toString());
			pstmt.setInt(1, end);
			pstmt.setInt(2, start);
			rs = pstmt.executeQuery();
			while(rs.next()){
				BoardVO vo = new BoardVO(rs.getInt("bno"), rs.getString("title"), rs.getString("contents"), rs.getString("writer"), rs.getString("ip"), rs.getInt("hits"), rs.getString("wdate"));
				list.add(vo);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	public BoardVO getData(int bno) {
		BoardVO vo = null;
		sql.setLength(0);
		sql.append("select bno, title, contents, writer, ip, hits, wdate ");
		sql.append("from board ");
		sql.append("where bno=?");
		
		try {
			pstmt = conn.prepareStatement(sql.toString());
			pstmt.setInt(1, bno);
			rs = pstmt.executeQuery();
			rs.next();
			vo = new BoardVO(rs.getInt("bno"), rs.getString("title"), rs.getString("contents"), rs.getString("writer"),
					rs.getString("ip"), rs.getInt("hits"), rs.getString("wdate"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return vo;
	}

	public void insertData(BoardVO vo){
		sql.setLength(0);
		sql.append("insert into board ");
		sql.append("values(board_bno_seq.nextval, ?, ?, ?, ?, ?, sysdate)");
		
		try {
			pstmt = conn.prepareStatement(sql.toString());
			pstmt.setString(1, vo.getTitle());
			pstmt.setString(2, vo.getContents());
			pstmt.setString(3, vo.getWriter());
			pstmt.setString(4, vo.getIp());
			pstmt.setInt(5, vo.getHits());
			pstmt.executeUpdate();
			System.out.println("Insert 완료");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void deleteData(int bno){
		sql.setLength(0);
		sql.append("delete from board ");
		sql.append("where bno=?");
		
		try {
			pstmt = conn.prepareStatement(sql.toString());
			pstmt.setInt(1, bno);
			pstmt.executeUpdate();
			System.out.println("Delete 완료");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void updateData(BoardVO vo){
		sql.setLength(0);
		sql.append("update board ");
		sql.append("set title=?, contents=?, writer=?, ip=?, hits=?, wdate=?");
		
		try {
			pstmt = conn.prepareStatement(sql.toString());
			pstmt.setString(1, vo.getTitle());
			pstmt.setString(2, vo.getContents());
			pstmt.setString(3, vo.getWriter());
			pstmt.setString(4, vo.getIp());
			pstmt.setInt(5, vo.getHits());
			pstmt.setString(6, vo.getWdate());
			pstmt.executeUpdate();
			System.out.println("Update 완료");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
