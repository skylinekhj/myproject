<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>   
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style type="text/css">
table{
 margin:0 auto;
 text-align:center;
 }
 table, tr, th, td{
   border:1px solid black;
   border-collapse:collapse;
 }
#col1{width:10%;}
#col2{width:40%;}
#col3{width:20%;}
#col4{width:10%;}
#col5{width:20%;}
</style>
</head>
<body>
    <table>
    	<tr>
    	    <th id="col1">게시물번호</th>
    	    <th id="col2">제목</th>
    		<th id="col3">작성자</th>
    		<th id="col4">조회수</th>
    		<th id="col5">작성일</th>
    		
    	</tr>
    	<c:forEach var="vo" items="${list}">
    	<tr>
    	<td>${vo.bno }</td>
    	<td><a href="board.do?cmd=detail&bno=${vo.bno}">
    	    ${vo.title }</a></td>
    	<td>${vo.writer }</td>
    	<td>${vo.hits }</td>
    	<td>${vo.wdate }</td>
    	</tr>
    	</c:forEach>
    </table>
    <a href="board.do?cmd=insertForm">
    <img src="images/btn_add.jpg" alt="글쓰기"/>
    </a>
</body>
</html>