<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style type="text/css">
table{
 width:1000px;
 margin:0 auto;
}
table, tr, th, td{
border:1px solid black;
border-collapse:collapse;
}
th{
width:100px;
}
#contents{
height:200px;
}
</style>
</head>
<body>
<table>
<tr>
<th>작성자</th>		
<td>${vo.writer }</td>
<th>작성일</th>
<td>${vo.wdate }</td>
</tr>
<tr>
<th>제목</th>
<td>${vo.title }</td>
<th>조회수</th>
<td>${vo.hits }</td>
</tr>
<tr id="contents">
<th>내용</th> 
<td colspan="3">${vo.contents }</td>
</tr>
<tr>
<td colspan="4">
 <input type="button" value="수정" />
 <input type="button" value="목록" />
 </td>
 </tr>
</table>
</body>
</html>